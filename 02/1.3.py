def fib_mod(n, m):
    per = [0, 1]
    while per[len(per) - 2] != 0 or per[len(per) - 1] != 1 or len(per) == 2:
        per.append((per[len(per) - 2] + per[len(per) - 1]) % m)
    return per[n % (len(per) - 2)]


def main():
    n, m = map(int, input().split())
    print(fib_mod(n, m))


if __name__ == "__main__":
    main()
