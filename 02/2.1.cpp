#include <iostream>

using namespace std;

class NOD
{
public:
    static int find(int a, int b) {
        while (true) {
            if (a == 0) return b;
            if (b == 0) return a;

            if (a > b) {
                a %= b;
            } else {
                b %= a;
            }
        }
    }
};

int main()
{
    int a = 0, b = 0;
    cin >> a >> b;
    cout << NOD::find(a, b) << endl;
    return 0;
}

