#include <iostream>

using namespace std;

class Fibonacci 
{
public:
    static int getLastDigit(int n) {
        if (n == 0) return 0;

        int i1 = 0, i2 = 1;
        for (int i = 1; i < n; ++i) {
            int temp = i2;
            i2 = (i1 + i2) % 10;
            i1 = temp;
        }

        return i2;
    }
};

int main()
{
    int n = 0;
    cin >> n;
    cout << Fibonacci::getLastDigit(n) << endl;
    return 0;
}

