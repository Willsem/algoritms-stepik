#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

typedef pair< int, int > Segment;
typedef vector< Segment > SegmentArray;
typedef vector< int > PointArray;

class SegmentCover
{
public:
    static bool comp(Segment l, Segment r) { return l.second < r.second; }
    static PointArray findPoints(SegmentArray segments)
    {
        sort(segments.begin(), segments.end(), comp);

        PointArray points;
        for (int i = 0; i < segments.size(); ++i) {
            points.push_back(segments[i].second);

            while (segments[i].first <= points[points.size() - 1] && 
                   segments[i].second >= points[points.size() - 1]) { i++;}
            --i;
        }

        return points;
    }
};

int main()
{
    int countSegments = 0;
    cin >> countSegments;
    SegmentArray segments(countSegments);
    for (Segment& seg : segments) {
        cin >> seg.first >> seg.second;
    }

    PointArray points = SegmentCover::findPoints(segments);

    cout << points.size() << endl;
    for (int point : points) {
        cout << point << ' ';
    }
    cout << endl;
    return 0;
}

